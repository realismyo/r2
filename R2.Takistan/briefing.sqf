// R2 - OA Briefing.
// Credits: F2, Edits by Simwah/Agent
// Even though the sections are listed backwards, they will appear the right way (SMEAC) in game.
// ====================================================================================
// JIP CHECK
// Prevents the script executing until the player has synchronised correctly:

#include "scripts\f_waitForJIP.sqf"

// ====================================================================================
// NOTES: CREDITS
// The code below creates the administration sub-section of notes.

player createDiaryRecord ["diary", ["Credits","
<br/>
*** Insert mission credits here. ***
<br/><br/>
Made with R2 (http://code.realismyo.com/r2)
"]];

// ====================================================================================
// NOTES: COMMAND
// The code below creates the administration sub-section of notes.

player createDiaryRecord ["diary", ["Command & Signal","
<br/>
*** Insert information on command and signal here. ***
"]];

// ====================================================================================
// NOTES: ADMINISTRATION
// The code below creates the administration sub-section of notes.

player createDiaryRecord ["diary", ["Administration","
<br/>
*** Insert information on administration and logistics here. ***
"]];

// ====================================================================================
// NOTES: EXECUTION
// The code below creates the execution sub-section of notes.

player createDiaryRecord ["diary", ["Execution","
<br/>
COMMANDER'S INTENT
<br/>
*** Insert very short summary of plan here. ***
<br/><br/>
MOVEMENT PLAN
<br/>
*** Insert movement instructions here. ***
<br/><br/>
FIRE SUPPORT PLAN
<br/>
*** Insert fire support instructions here. ***
<br/><br/>
SPECIAL TASKS
<br/>
*** Insert instructions for specific units here. ***
"]];

// ====================================================================================
// NOTES: MISSION
// The code below creates the mission sub-section of notes.

player createDiaryRecord ["diary", ["Mission","
<br/>
*** Insert the mission here. ***
"]];

// ====================================================================================
// NOTES: SITUATION
// The code below creates the situation sub-section of notes.

player createDiaryRecord ["diary", ["Situation","
<br/>
*** Insert general information about the situation here.***
<br/><br/>
ENEMY FORCES
<br/>
*** Insert information about enemy forces here.***
<br/><br/>
FRIENDLY FORCES
<br/>
*** Insert information about friendly forces here.***
"]];

// ====================================================================================