// R2 - init.sqf
//===================================================================================
// ACE settings

ace_sys_eject_fnc_weaponcheck = {};							// Disable ACE from removing weapons in vehicles.
ace_sys_wounds_no_medical_gear = true;						// Removed default medical gear so the assign gear scripts control everything.
ace_settings_enable_vd_change = true;						// Enable ACE wiewdistance change in ACE options.
//===================================================================================
// Module Checking Script
// Read script for notes on how to disable this function

if (isServer) then { execVM "scripts\moduleCheck.sqf"; };
//===================================================================================
// Settings for Dead Cleanup
// [60,0,true] execVM "scripts\cly_removedead.sqf";
// [wait time for men,wait time for vehicles,remove units with gear (optional, default true)] execVM "scripts\cly_removedead.sqf";
// A wait time of 0 prevents that type from being removed.
// Prevent an individual unit from being removed:  this setVariable ["CLY_removedead",false,true]
// Remove an individual unit immediately upon death:  this setVariable ["CLY_removedead",true,true]

[180,0,true] execVM "scripts\cly_removedead.sqf";
//===================================================================================
// Settings for Bleedout Timer
// Set the number of seconds for the bleedout timer to last.

ace_wounds_prevtime = 180;
//===================================================================================
// Disable BIS Conversations

player setVariable ["BIS_noCoreConversations", true];
//===================================================================================
// Disable Saving and Auto Saving

enableSaving [false, false];
//=================================================================================== 
// R2 - Respawn INIT
// Change this to match the assignGear script you're using.

assignGearScript = compile preprocessfilelinenumbers "scripts\assignGear_SOTG.sqf";
f_respawnINIT = player addEventHandler ["killed", {_this execVM "init_onPlayerRespawn.sqf"}];
// ==================================================================================
// R2 - Cratepicker INIT
// Set to false if wanting to use custom cargo in spawned cargo crates.
// Define custom cargo in "cratePicker_supplyCrates.sqf", in the cratePicker folder.

if (isServer) then { agent_var_cratePicker_changePerSide = true; publicVariable "agent_var_cratePicker_changePerSide"; };
// ==================================================================================
// R2 - Mission Timer/Safe Start
// Set this to to the number of minutes you wish the safe start timer to last.
// Disable this section if you don't want to use this function.

f_param_mission_timer = 5;
[] execVM "scripts\safeStart\f_safeStart.sqf";
//===================================================================================
// R2 - CO Briefing
// Edit briefing.sqf with your intended SMEAC

[] execVM "briefing.sqf";
//===================================================================================
// Settings for MURK
// See first_group.sqf for the actual trigger condition, middle of the file

first_group = false;
second_group = false;
third_group = false;
fourth_group = false;
fifth_group = false;
//===================================================================================