// R2 - Safe Start, Safety Toggle
// Credits: F2, Edits by Simwah
//=====================================================================================
// Exit if server
if(isDedicated) exitwith {};

_switch = _this select 0;
switch (_switch) do {

	// Turn safety on
	case true : {
		// Delete bullets from fired weapons
		f_safety = player addEventHandler["Fired", {deletevehicle (_this select 6);}];
		
		// Make playable units invincible, clientside
		{
			_x allowDamage false;
			//_x setVariable ["ace_w_allow_dam", false, true];
		} foreach playableunits;
	};
	
	// Turn safety off
	case false : {
	
		// Allow player to fire weapons
		player removeEventHandler ["Fired", f_safety];
		
		// Make playable units vulnerable, clientside
		{
			_x allowDamage true;
			//_x setVariable ["ace_w_allow_dam", nil, true];
		} foreach playableunits;
	};
};
