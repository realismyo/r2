// R2 - Safe Start, Client Hint
// Credits: F2, Edits by Simwah
// ====================================================================================
// Run the hint on all clients

if(!isDedicated) then {

	// display hint while timer is active
	while {pv_mission_timer > 0} do {
		// hintsilent  ["SAFE START\n(s)\n", pv_mission_timer];
		hintSilent parseText format ["<t size='2.0'>Weapons are on Safe!<br/>Time Remaining: %1 min",pv_mission_timer];
		if (pv_mission_timer == 0) exitWith {};
 		sleep 20;
	};

	// Display notification when the mission starts
	hintSilent parseText format ["<t size='2.0'>Weapons are free<br/>Be careful!"];
};
