/* 
F/A-18 Loadout Script
by Stevetastic
Inspired by AGeNT

====== USAGE ======
_nul = [VEHICLE,LOADOUT,SERVICE MENU] execVM "scripts\fa18Loadout.sqf";
- VEHICLE
    The name of the vehicle. use THIS to allow ARMA to set it dynamically.
- LOADOUT
    The loadout profile. Choose between the following available loadouts.
    "casBalanced",
    "casAgmHeavy",
    "casGbuHeavy",
    "multirole",
    "cap"
- SERVICE MENU
    Choose whether the in game service menu is accessible. set either TRUE or FALSE.
EXAMPLE: _nul = [this,"casBalanced",true] execVM "scripts\fa18Loadout.sqf";

- Loadouts can be edited below if the currently available loadouts aren't suitable for your mission.
====================
*/
private ["_load"];
_unit = _this select 0;
_loadout = toLower (_this select 1);
_serviceMenu = _this select 2;

// ===== ORDNANCE CLASSNAMES =====
//Cannon					//Magazines
_cannon = "js_w_fa18_m61";			_cannonMag = "js_m_fa18_m61";

// Guided missiles				//Magazines
_aim9x = "js_w_fa18_aim9xLaucher";		_aim9xMag1 = "js_m_fa18_aim9x_x1";
						_aim9xMag2 = "js_m_fa18_aim9x_x2";
_aim120 = "js_w_fa18_aim120cLaucher";		_aim120Mag1 = "js_m_fa18_aim120c_x1";
						_aim120Mag2 = "js_m_fa18_aim120c_x2";
						_aim120Mag4 = "js_m_fa18_aim120c_x4";
_agm65 = " js_w_fa18_MaverickLauncher";		_agm65Mag1 = "js_m_fa18_Maverick_x1";
						_agm65Mag2 = "js_m_fa18_Maverick_x2";
						_agm65Mag4 = "js_m_fa18_Maverick_x4";
_agm84 = "js_w_fa18_HarpoonLauncher";		_agm84Mag = "js_m_fa18_Harpoon_x1";
						_agm84Mag = "js_m_fa18_Harpoon_x2";

//Bombs						//Magazines
_gbu12 = "js_w_fa18_GBU12LGBLauncher";		_gbu12Mag1 = "js_m_fa18_GBU12_x1";
						_gbu12Mag4 = "js_m_fa18_GBU12_x4";
_gbu31JDAM = "js_w_fa18_GBU31BombLauncher";	_gbu31Mag = "js_m_fa18_GBU31_JDAM_x1";
_gbu32JDAM = "js_w_fa18_GBU32BombLauncher";	_gbu32Mag = "js_m_fa18_GBU32_JDAM_x1";
_gbu38JDAM = "js_w_fa18_GBU38BombLauncher";	_gbu38Mag = "js_m_fa18_GBU38_JDAM_x1";
_mk82 = "js_w_fa18_Mk82BombLauncher";		_mk82Mag2 = "js_m_fa18_MK82_x2";
						_mk82Mag4 = "js_m_fa18_MK82_x4";

//Misc						//Magazines
_fuelTank = "js_w_fa18_fueltank_holder";	_fuelTankMag = "js_m_fa18_wing_tank_x1";
_safety = "js_w_master_arms_safe";		
						_dualBombRack = "js_m_fa18_bombrack_x1";
						_emptRack = "js_m_fa18_empty";
						_buddyPod = "js_m_fa18_buddypod_x1";
						_altflirTGP = "js_m_fa18_altflir_pod_x1";

// ===== LOADOUTS =====
// These loadouts are in the form of a lists. Maintain the format if you want any custom loadouts to work.

_loadoutcasbalancedE = [
//Station 1
"js_m_fa18_aim9x_x1",
//Station 2
"js_m_fa18_aim9x_x1",
//Station 3
"js_m_fa18_GBU12_x1",
//Station 4
"js_m_fa18_GBU12_x1",
//Station 5 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 6 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 7 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 8 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 9
"js_m_fa18_empty",
//Station 10
"js_m_fa18_empty",
//Station 11 USED IF Station 7 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 12 USED IF Station 8 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 13 USED IF Station 7 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 14 USED IF Station 8 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 15  USED IF Station 5 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 16 USED IF Station 6 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 17 USED IF Station 5 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 18 USED IF Station 6 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 19
"js_m_fa18_wing_tank_x1"
];

_loadoutcasbalancedF = [
//Station 1
"js_m_fa18_aim9x_x1",
//Station 2
"js_m_fa18_aim9x_x1",
//Station 3
"js_m_fa18_GBU12_x1",
//Station 4
"js_m_fa18_GBU12_x1",
//Station 5 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 6 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 7 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 8 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 9
"js_m_fa18_empty",
//Station 10
"js_m_fa18_altflir_pod_x1",
//Station 11 USED IF Station 7 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 12 USED IF Station 8 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 13 USED IF Station 7 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 14 USED IF Station 8 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 15  USED IF Station 5 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 16 USED IF Station 6 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 17 USED IF Station 5 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 18 USED IF Station 6 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 19
"js_m_fa18_wing_tank_x1"
];

_loadoutcasagmheavyE = [
//Station 1
"js_m_fa18_aim9x_x1",
//Station 2
"js_m_fa18_aim9x_x1",
//Station 3
"js_m_fa18_GBU12_x1",
//Station 4
"js_m_fa18_GBU12_x1",
//Station 5 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 6 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 7 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 8 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 9
"js_m_fa18_empty",
//Station 10
"js_m_fa18_empty",
//Station 11 USED IF Station 7 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 12 USED IF Station 8 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 13 USED IF Station 7 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 14 USED IF Station 8 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 15  USED IF Station 5 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 16 USED IF Station 6 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 17 USED IF Station 5 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 18 USED IF Station 6 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 19
"js_m_fa18_wing_tank_x1"
];

_loadoutcasagmheavyF = [
//Station 1
"js_m_fa18_aim9x_x1",
//Station 2
"js_m_fa18_aim9x_x1",
//Station 3
"js_m_fa18_GBU12_x1",
//Station 4
"js_m_fa18_GBU12_x1",
//Station 5 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 6 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 7 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 8 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 9
"js_m_fa18_empty",
//Station 10
"js_m_fa18_altflir_pod_x1",
//Station 11 USED IF Station 7 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 12 USED IF Station 8 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 13 USED IF Station 7 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 14 USED IF Station 8 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 15  USED IF Station 5 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 16 USED IF Station 6 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 17 USED IF Station 5 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 18 USED IF Station 6 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 19
"js_m_fa18_wing_tank_x1"
];

_loadoutcasgbuheavyE = [
//Station 1
"js_m_fa18_aim9x_x1",
//Station 2
"js_m_fa18_aim9x_x1",
//Station 3
"js_m_fa18_Maverick_x1",
//Station 4
"js_m_fa18_Maverick_x1",
//Station 5 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 6 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 7 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 8 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 9
"js_m_fa18_empty",
//Station 10
"js_m_fa18_empty",
//Station 11 USED IF Station 7 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 12 USED IF Station 8 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 13 USED IF Station 7 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 14 USED IF Station 8 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 15  USED IF Station 5 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 16 USED IF Station 6 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 17 USED IF Station 5 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 18 USED IF Station 6 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 19
"js_m_fa18_wing_tank_x1"
];

_loadoutcasgbuheavyF = [
//Station 1
"js_m_fa18_aim9x_x1",
//Station 2
"js_m_fa18_aim9x_x1",
//Station 3
"js_m_fa18_Maverick_x1",
//Station 4
"js_m_fa18_Maverick_x1",
//Station 5 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 6 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 7 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 8 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 9
"js_m_fa18_empty",
//Station 10
"js_m_fa18_altflir_pod_x1",
//Station 11 USED IF Station 7 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 12 USED IF Station 8 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 13 USED IF Station 7 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 14 USED IF Station 8 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 15  USED IF Station 5 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 16 USED IF Station 6 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 17 USED IF Station 5 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 18 USED IF Station 6 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 19
"js_m_fa18_wing_tank_x1"
];

_loadoutcapE = [
//Station 1
"js_m_fa18_aim9x_x1",
//Station 2
"js_m_fa18_aim9x_x1",
//Station 3
"js_m_fa18_aim9x_x1",
//Station 4
"js_m_fa18_aim9x_x1",
//Station 5 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 6 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 7 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 8 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 9
"js_m_fa18_aim120c_x1",
//Station 10
"js_m_fa18_aim120c_x1",
//Station 11 USED IF Station 7 IS DUAL RAIL
"js_m_fa18_aim120c_x1",
//Station 12 USED IF Station 8 IS DUAL RAIL
"js_m_fa18_aim120c_x1",
//Station 13 USED IF Station 7 IS DUAL RAIL
"js_m_fa18_aim120c_x1",
//Station 14 USED IF Station 8 IS DUAL RAIL
"js_m_fa18_aim120c_x1",
//Station 15  USED IF Station 5 IS DUAL RAIL
"js_m_fa18_aim120c_x1",
//Station 16 USED IF Station 6 IS DUAL RAIL
"js_m_fa18_aim120c_x1",
//Station 17 USED IF Station 5 IS DUAL RAIL
"js_m_fa18_aim120c_x1",
//Station 18 USED IF Station 6 IS DUAL RAIL
"js_m_fa18_aim120c_x1",
//Station 19
"js_m_fa18_wing_tank_x1"
];

_loadoutcapF = [
//Station 1
"js_m_fa18_aim9x_x1",
//Station 2
"js_m_fa18_aim9x_x1",
//Station 3
"js_m_fa18_aim9x_x1",
//Station 4
"js_m_fa18_aim9x_x1",
//Station 5 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 6 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 7 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 8 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 9
"js_m_fa18_altflir_pod_x1",
//Station 10
"js_m_fa18_aim120c_x1",
//Station 11 USED IF Station 7 IS DUAL RAIL
"js_m_fa18_aim120c_x1",
//Station 12 USED IF Station 8 IS DUAL RAIL
"js_m_fa18_aim120c_x1",
//Station 13 USED IF Station 7 IS DUAL RAIL
"js_m_fa18_aim120c_x1",
//Station 14 USED IF Station 8 IS DUAL RAIL
"js_m_fa18_aim120c_x1",
//Station 15  USED IF Station 5 IS DUAL RAIL
"js_m_fa18_aim120c_x1",
//Station 16 USED IF Station 6 IS DUAL RAIL
"js_m_fa18_aim120c_x1",
//Station 17 USED IF Station 5 IS DUAL RAIL
"js_m_fa18_aim120c_x1",
//Station 18 USED IF Station 6 IS DUAL RAIL
"js_m_fa18_aim120c_x1",
//Station 19
"js_m_fa18_wing_tank_x1"
];

_loadoutmultiroleE = [
//Station 1
"js_m_fa18_aim9x_x1",
//Station 2
"js_m_fa18_aim9x_x1",
//Station 3
"js_m_fa18_aim120c_x1",
//Station 4
"js_m_fa18_aim120c_x1",
//Station 5 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 6 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 7 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 8 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 9
"js_m_fa18_aim120c_x1",
//Station 10
"js_m_fa18_altflir_pod_x1",
//Station 11 USED IF Station 7 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 12 USED IF Station 8 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 13 USED IF Station 7 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 14 USED IF Station 8 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 15  USED IF Station 5 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 16 USED IF Station 6 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 17 USED IF Station 5 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 18 USED IF Station 6 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 19
"js_m_fa18_wing_tank_x1"
];

_loadoutmultiroleF = [
//Station 1
"js_m_fa18_aim9x_x1",
//Station 2
"js_m_fa18_aim9x_x1",
//Station 3
"js_m_fa18_aim120c_x1",
//Station 4
"js_m_fa18_aim120c_x1",
//Station 5 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 6 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 7 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 8 DUAL RAIL COMPATIBLE
"js_m_fa18_bombrack_x1",
//Station 9
"js_m_fa18_aim120c_x1",
//Station 10
"js_m_fa18_aim120c_x1",
//Station 11 USED IF Station 7 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 12 USED IF Station 8 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 13 USED IF Station 7 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 14 USED IF Station 8 IS DUAL RAIL
"js_m_fa18_Maverick_x1",
//Station 15  USED IF Station 5 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 16 USED IF Station 6 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 17 USED IF Station 5 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 18 USED IF Station 6 IS DUAL RAIL
"js_m_fa18_GBU12_x1",
//Station 19
"js_m_fa18_wing_tank_x1"
];

// ==================================================
// ========== DO NOT EDIT BELOW THIS LINE ===========
// ==================================================
_removeWeapons = 
{
_unit removeWeapon "js_w_fa18_aim9xLaucher"; 
_unit removeWeapon "js_w_fa18_aim120cLaucher"; 
_unit removeWeapon "js_w_fa18_GBU12LGBLaucher";
_unit removeWeapon "js_w_fa18_Mk82BombLauncher";  
_unit removeWeapon "js_w_fa18_MaverickLauncher"; 
_unit removeWeapon "js_w_fa18_fueltank_holder"; 
_unit removeWeapon "js_w_fa18_HarpoonLauncher";
_unit removeWeapon "js_w_fa18_GBU31BombLauncher";
_unit removeWeapon "js_w_fa18_GBU32BombLauncher";
_unit removeWeapon "js_w_fa18_GBU38BombLauncher";
_unit removeWeapon "Laserdesignator_mounted";

_unit removeWeapon "GLT_AIM9X_Launcher"; 
_unit removeWeapon "GLT_AIM120_Launcher";
_unit removeWeapon "GLT_AGM65_Launcher";  
_unit removeWeapon "GLT_GBU12_Launcher"; 
_unit removeWeapon "GLT_GBU53_Launcher"; 
_unit removeWeapon "GLT_AGM84_Launcher";

_unit removeMagazines "GLT_2Rnd_AIM9X";
_unit removeMagazines "GLT_2Rnd_AIM120"; 
_unit removeMagazines "GLT_4Rnd_AIM120"; 
_unit removeMagazines "GLT_2Rnd_AGM65"; 
_unit removeMagazines "GLT_4Rnd_AGM65"; 
_unit removeMagazines "GLT_4Rnd_GBU12";
_unit removeMagazines "GLT_2Rnd_GBU53"; 
_unit removeMagazines "GLT_4Rnd_GBU53";  
_unit removeMagazines "GLT_6Rnd_GBU53";
_unit removeMagazines "GLT_2Rnd_AGM84";

_unit removeMagazines "js_m_fa18_aim9x_x1";
_unit removeMagazines "js_m_fa18_aim9x_x2";
_unit removeMagazines "js_m_fa18_aim120c_x1"; 
_unit removeMagazines "js_m_fa18_aim120c_x2"; 
_unit removeMagazines "js_m_fa18_aim120c_x4"; 
_unit removeMagazines "js_m_fa18_bombrack_x1";
_unit removeMagazines "js_m_fa18_bombrack_x2"; 
_unit removeMagazines "js_m_fa18_GBU12_x1";
_unit removeMagazines "js_m_fa18_GBU12_x4";
_unit removeMagazines "js_m_fa18_MK82_x1";
_unit removeMagazines "js_m_fa18_MK82_x2"; 
_unit removeMagazines "js_m_fa18_MK82_x4";
_unit removeMagazines "js_m_fa18_GBU38_JDAM_x1";
_unit removeMagazines "js_m_fa18_GBU32_JDAM_x1"; 
_unit removeMagazines "js_m_fa18_GBU31_JDAM_x1";
_unit removeMagazines "js_m_fa18_Maverick_x1";   
_unit removeMagazines "js_m_fa18_Maverick_x2"; 
_unit removeMagazines "js_m_fa18_Maverick_x4";
_unit removeMagazines "js_m_fa18_Harpoon_x1";
_unit removeMagazines "js_m_fa18_Harpoon_x2";
_unit removeMagazines "js_m_fa18_wing_tank_x1";
_unit removeMagazines "js_m_fa18_wing_tank_x2";
_unit removeMagazines "js_m_fa18_altflir_pod_x1";  
_unit removeMagazines "js_m_fa18_empty"; 
_unit removeMagazines "js_m_fa18_fake_empty";
_unit removeMagazines "js_m_fa18_buddypod_x1";
_unit removeMagazines "Laserbatteries";
};
_addWeapons =
{
sleep 0.5;
if (("js_m_fa18_aim9x_x1" in _load) or ("js_m_fa18_aim9x_x2" in _load)) then {_unit addWeapon "js_w_fa18_aim9xLaucher";};
if (("js_m_fa18_aim120c_x1" in _load) or ("js_m_fa18_aim120c_x2" in _load) or ("js_m_fa18_aim120c_x4" in _load)) then {_unit addWeapon "js_w_fa18_aim120cLaucher";};
if (("js_m_fa18_GBU12_x1" in _load) or ("js_m_fa18_GBU12_x4" in _load)) then {_unit addWeapon "js_w_fa18_GBU12LGBLaucher";};
if (("js_m_fa18_MK82_x1" in _load) or ("js_m_fa18_MK82_x2" in _load) or ("js_m_fa18_MK82_x4" in _load)) then {_unit addWeapon "js_w_fa18_Mk82BombLauncher";};
if (("js_m_fa18_GBU38_JDAM_x1" in _load)) then {_unit addWeapon "js_w_fa18_GBU38BombLauncher";};
if (("js_m_fa18_GBU32_JDAM_x1" in _load)) then {_unit addWeapon "js_w_fa18_GBU32BombLauncher";};
if (("js_m_fa18_GBU31_JDAM_x1" in _load)) then {_unit addWeapon "js_w_fa18_GBU31BombLauncher";};
if (("js_m_fa18_Maverick_x1" in _load) or ("js_m_fa18_Maverick_x2" in _load) or ("js_m_fa18_Maverick_x4" in _load)) then {_unit addWeapon "js_w_fa18_MaverickLauncher";};
if (("js_m_fa18_Harpoon_x1" in _load) or ("js_m_fa18_Harpoon_x2" in _load)) then {_unit addWeapon "js_w_fa18_HarpoonLauncher";};
if (("js_m_fa18_wing_tank_x1" in _load) or ("js_m_fa18_wing_tank_x2" in _load)) then {_unit addWeapon "js_w_fa18_fueltank_holder";};
if (("js_m_fa18_altflir_pod_x1" in _load)) then {_unit addWeapon "Laserdesignator_mounted"; _unit addMagazine "Laserbatteries"};
};

_calcFuel =
{
_Fuel_tank_count = {_x == "js_m_fa18_wing_tank_x1"} count magazines _unit;
If (_Fuel_tank_count < 1) Then 
{	
	_External_fuel = _unit animationPhase "auxtank_switch";
	_unit animate ["auxtank_switch",0];
};

If (_Fuel_tank_count == 1) Then 
{	
	_External_fuel = _unit animationPhase "auxtank_switch";
	If ((_External_fuel < 0.2)) Then 
	{
		_unit animate ["auxtank_switch",(_External_fuel + (0.2 - _External_fuel))];
	};
	If ((_External_fuel > 0.2)) Then 
	{
		_unit animate ["auxtank_switch",0.2];
	};
};

If (_Fuel_tank_count == 2) Then 
{	
	_External_fuel = _unit animationPhase "auxtank_switch";
	If ((_External_fuel < 0.4)) Then 
	{
		_unit animate ["auxtank_switch",(_External_fuel + (0.4 - _External_fuel))];
	};
	If ((_External_fuel > 0.4)) Then 
	{
		_unit animate ["auxtank_switch",0.4];
	};
};

If (_Fuel_tank_count == 3) Then 
{	
	_External_fuel = _unit animationPhase "auxtank_switch";
	If ((_External_fuel < 0.6)) Then 
	{
		_unit animate ["auxtank_switch",(_External_fuel + (0.6 - _External_fuel))];
	};
	If ((_External_fuel > 0.6)) Then 
	{
		_unit animate ["auxtank_switch",0.6];
	};
};

If (_Fuel_tank_count == 4) Then 
{	
	_External_fuel = _unit animationPhase "auxtank_switch";
	If ((_External_fuel < 0.8)) Then 
	{
		_unit animate ["auxtank_switch",(_External_fuel + (0.8 - _External_fuel))];
	};
	If ((_External_fuel > 0.8)) Then 
	{
		_unit animate ["auxtank_switch",0.8];
	};
};

If (_Fuel_tank_count == 5) Then 
{	
	_External_fuel = _unit animationPhase "auxtank_switch";
	If ((_External_fuel < 0.99)) Then 
	{
		_unit animate ["auxtank_switch",(_External_fuel + (1.0 - _External_fuel))];
	};
	If ((_External_fuel >= 1.0)) Then 
	{
		_unit animate ["auxtank_switch",1.0];
	};
};

sleep 0.05;
_External_fuel = _unit animationPhase "auxtank_switch";

If (("js_m_fa18_buddypod_x1" in _load)) Then 
{	
	
	If (_Fuel_tank_count > 1) Then 
	{	
		_unit animate ["auxtank_switch",(_External_fuel + 0.2)];
	};
	If (_Fuel_tank_count < 1) Then 
	{	
		_unit animate ["auxtank_switch",0.2];
	};

};
};
if (_unit != _unit) then {waitUntil {!isNull _unit}};	// make sure unit exists, or wait until it does

if(_serviceMenu) then
{
    FA18_SERVICE_OPTIONS = true;
} else
{
    FA18_SERVICE_OPTIONS = false;
};

[] call _removeWeapons;
sleep 1;

switch (_loadout) do {					// case switch for desired loadout
	// CAS - Balanced
		case "casbalanced" : {
			if(_unit isKindOf "js_fa18e") then
			{
				{_unit addmagazine _x} foreach _loadoutcasbalancedE;
			};
			if(_unit isKindOf "js_fa18f") then
			{
				{_unit addmagazine _x} foreach _loadoutcasbalancedF;
			};
	};
	// CAS - AGM heavy
		case "casagmheavy" : {
			if(_unit isKindOf "js_fa18e") then
			{
				{_unit addmagazine _x} foreach _loadoutcasagmheavyE;
			};
			if(_unit isKindOf "js_fa18f") then
			{
				{_unit addmagazine _x} foreach _loadoutcasagmheavyF;
			};
	};
	// CAS - GBU heavy
		case "casgbuheavy" : {
			if(_unit isKindOf "js_fa18e") then
			{
				{_unit addmagazine _x} foreach _loadoutcasgbuheavyE;
			};
			if(_unit isKindOf "js_fa18f") then
			{
				{_unit addmagazine _x} foreach _loadoutcasgbuheavyF;
			};
	};
	// CAP
		case "cap" : {
			if(_unit isKindOf "js_fa18e") then
			{
				{_unit addmagazine _x} foreach _loadoutcapE;
			};
			if(_unit isKindOf "js_fa18f") then
			{
				{_unit addmagazine _x} foreach _loadoutcapF;
			};
	};
	// Multi Role
		case "multirole" : {
			if(_unit isKindOf "js_fa18e") then
			{
				{_unit addmagazine _x} foreach _loadoutmultiroleE;
			};
			if(_unit isKindOf "js_fa18f") then
			{
				{_unit addmagazine _x} foreach _loadoutmultiroleF;
			};
	};
	// if undefined or incorrectly defined do nothing.
	default {
	};
};
_load = magazines _unit;
[] call _addWeapons;
sleep 1;
[] call _calcFuel;
_unit setVehicleAmmo 1;
_unit selectWeapon "js_w_master_arms_safe";
_unit animate ["rearming_done_switch",1];
Exit;