// ACE Module Checker Script
// By AGeNT
// Set variables to false to disable certain elements, or everything.

private ["_setVariables","_checkModules","_enableOnFalse","_allMedics","_fullHeal","_cookOff","_spareTyres"];

_setVariables = true;			// set ace variables from framework
_checkModules = true;			// enable modules
_enableOnFalse = true;			// enable modules if variables are false

_allMedics = true;				// spawn module for all players can use medkits
_fullHeal = true;				// spawn module for full heal in field
_cookOff = true;				// spawn ammo cook off module
_spareTyres = true;				// spawn spare tyres in vehicles module

if (!isServer) exitWith {};		// exit if not server

if (_setVariables) then {
	ace_sys_eject_fnc_weaponcheck = {}; publicVariable "ace_sys_eject_fnc_weaponcheck";										// disable ACE removing weapons in vehicles
	ace_sys_wounds_no_medical_gear = true; publicVariable "ace_sys_wounds_no_medical_gear";									// disable ACE adding medical gear
	ace_settings_enable_vd_change = true; publicVariable "ace_settings_enable_vd_change";									// enable ACE Viewdistance change in ACE options
};

if (_checkModules) then {

	private ["_center","_group","_pos"];
	
	_center = createCenter sideLogic;
	_group = createGroup _center;
	_pos = [0, 0, 0];
	
	if (isNil "ace_sys_wounds_enabled") then {
		private ["_woundModule","_allMedicsModule","_fullHealModule"];
		_woundModule = _group createUnit ["ACE_Wounds_Logic",_pos , [], 0, ""];												// ace_sys_wounds_enabled
		if (_allMedics) then { _allMedicsModule = _group createUnit ["ACE_Wounds_EveryoneMedic",_pos , [], 0, ""]; };		// ace_sys_wounds_all_medics
		if (_fullHeal) then { _fullHealModule = _group createUnit ["ACE_Wounds_FullHeal",_pos , [], 0, ""]; };				// ace_sys_wounds_leftdam
	};
	if (isNil "ace_sys_viewblock_blockgrass") then {
		private "_viewBlockModule";
		_viewBlockModule = _group createUnit ["ACE_Viewblock_EnableAIGrassVB_Mod",_pos , [], 0, ""];						// ace_sys_viewblock_blockgrass
	};
	if (isNil "ace_sys_destruction_enable_cookoff") then {
		private "_cookOffModule";
		if (_cookOff) then { _cookOffModule = _group createUnit ["ACE_Vehicledamage_Enable_Cookoff",_pos , [], 0, ""]; };	// ace_sys_destruction_enable_cookoff
	};
	if (isNil "ace_sys_repair_default_tyres") then {
		private "_tyreModule";
		if (_spareTyres) then { _tyreModule = _group createUnit ["ace_sys_repair_tyres",_pos , [], 0, ""]; };				// ace_sys_repair_default_tyres
	};	
};

if (_enableOnFalse) then {

	private ["_center","_group","_pos"];
	
	_center = createCenter sideLogic;
	_group = createGroup _center;
	_pos = [0, 0, 0];
	
	if (!ace_sys_wounds_enabled) then {
		private ["_woundModule","_allMedicsModule","_fullHealModule"];
		_woundModule = _group createUnit ["ACE_Wounds_Logic",_pos , [], 0, ""];												// ace_sys_wounds_enabled
		if (_allMedics) then { _allMedicsModule = _group createUnit ["ACE_Wounds_EveryoneMedic",_pos , [], 0, ""]; };		// ace_sys_wounds_all_medics
		if (_fullHeal) then { _fullHealModule = _group createUnit ["ACE_Wounds_FullHeal",_pos , [], 0, ""]; };				// ace_sys_wounds_leftdam
	};
	if (!ace_sys_viewblock_blockgrass) then {
		private "_viewBlockModule";
		_viewBlockModule = _group createUnit ["ACE_Viewblock_EnableAIGrassVB_Mod",_pos , [], 0, ""];						// ace_sys_viewblock_blockgrass
	};
	if (!ace_sys_destruction_enable_cookoff) then {
		private "_cookOffModule";
		if (_cookOff) then { _cookOffModule = _group createUnit ["ACE_Vehicledamage_Enable_Cookoff",_pos , [], 0, ""]; };	// ace_sys_destruction_enable_cookoff
	};
	if (!ace_sys_repair_default_tyres) then {
		private "_tyreModule";
		if (_spareTyres) then { _tyreModule = _group createUnit ["ace_sys_repair_tyres",_pos , [], 0, ""]; };				// ace_sys_repair_default_tyres
	};
};