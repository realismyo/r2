#define DLG (uiNamespace getVariable "KitPick_Dlg")
#define COMBO (uiNamespace getVariable "KitPick_Combo")
#define USEBUTTON (uiNamespace getVariable "KitPick_UseBtn")

KitPickInit =
{
    uiNamespace setVariable ["KitPick_Dlg", _this select 0];
    uiNamespace setVariable ["KitPick_Combo", (_this select 0) displayCtrl 1];
    uiNamespace setVariable ["KitPick_UseBtn", (_this select 0) displayCtrl 3];

    USEBUTTON ctrlEnable false;

    private "_idx";
    COMBO lbAdd "--Platoon Loadouts--";
    _idx = COMBO lbAdd "Platoon Commander"; COMBO lbSetData [_idx, "pltld"];
    _idx = COMBO lbAdd "Platoon Sergeant"; COMBO lbSetData [_idx, "pltld"];
    _idx = COMBO lbAdd "Platoon Medic"; COMBO lbSetData [_idx, "pltmed"];
    _idx = COMBO lbAdd "JTAC / FO"; COMBO lbSetData [_idx, "pltfac"];
    _idx = COMBO lbAdd "Rifleman"; COMBO lbSetData [_idx, "rm"];
    COMBO lbAdd "--Section Loadouts--";
    _idx = COMBO lbAdd "Section Commander"; COMBO lbSetData [_idx, "secco"];
    _idx = COMBO lbAdd "Fire Team Leader (2IC/3IC)"; COMBO lbSetData [_idx, "sectl"];
    _idx = COMBO lbAdd "Grenadier"; COMBO lbSetData [_idx, "gren"];
    _idx = COMBO lbAdd "Automatic Rifleman"; COMBO lbSetData [_idx, "ar"];
    _idx = COMBO lbAdd "Assistant Automatic Rifleman"; COMBO lbSetData [_idx, "aar"];
    _idx = COMBO lbAdd "Marksman"; COMBO lbSetData [_idx, "dmr"];
    _idx = COMBO lbAdd "Rifleman (AT)"; COMBO lbSetData [_idx, "rmat"];
    COMBO lbAdd "--Support Loadouts--";
    _idx = COMBO lbAdd "Light Machine Gunner"; COMBO lbSetData [_idx, "mmg"];
    _idx = COMBO lbAdd "Assistant Light Machine Gunner"; COMBO lbSetData [_idx, "mmgass"];
    _idx = COMBO lbAdd "Heavy Weapons Gunner"; COMBO lbSetData [_idx, "hmggun"];
    _idx = COMBO lbAdd "Heavy Weapons Assistant"; COMBO lbSetData [_idx, "hmgass"];
    _idx = COMBO lbAdd "Heavy Weapons Ammo Bearer"; COMBO lbSetData [_idx, "hmgammo"];
    _idx = COMBO lbAdd "Medium Anti-Tank Gunner"; COMBO lbSetData [_idx, "matgun"];
    _idx = COMBO lbAdd "Medium Anti-Tank Ammo Bearer"; COMBO lbSetData [_idx, "matammo"];
    _idx = COMBO lbAdd "Heavy Anti-Tank Gunner"; COMBO lbSetData [_idx, "hatgun"];
    _idx = COMBO lbAdd "Heavy Anti-Tank Ammo Bearer"; COMBO lbSetData [_idx, "hatammo"];
    COMBO lbAdd "--Crew Loadouts--";
    _idx = COMBO lbAdd "Pilot"; COMBO lbSetData [_idx, "pilot"];
    _idx = COMBO lbAdd "Crewman"; COMBO lbSetData [_idx, "crewman"];
	
    _sel = player getVariable "KitPicker_Selection";
    if (!isNil '_sel') then
    {
        COMBO lbSetCurSel _sel;
    };
};

KitPicker_Pick =
{
    private "_idx";
    _idx = lbCurSel COMBO;
    if (_idx < 0) exitWith {};

    _data = COMBO lbData _idx;
    if (_data == "") exitWith {};

    closeDialog 0;
    player setVariable ["f_var_JIP_loadout", _data];
    player setVariable ["KitPicker_Selection", _idx];
    f_var_JIP_state = 3;
};

KitPicker_OnSelChanged =
{
    private "_idx";
    _idx = lbCurSel COMBO;
    if (_idx < 0) exitWith
    {
        USEBUTTON ctrlEnable false;
    };

    _data = COMBO lbData _idx;
    if (_data == "") exitWith
    {
        USEBUTTON ctrlEnable false;
    };
    USEBUTTON ctrlEnable true;
};
