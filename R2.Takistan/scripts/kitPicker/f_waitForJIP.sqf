// R2 - Wait for JIP
// Credits: F2, Edits by Simwah
// ====================================================================================

if (!isDedicated && (player != player)) then
{
    waitUntil {player == player};
    waitUntil {time > 10};
};
