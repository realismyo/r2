/* 
Crate Filler Script
by AGeNT

- Designed to fill the ACE Empty Boxes with supplies to be dropped/transported by vehicle.
- Works by side. Has settings for blufor, opfor, indfor, a custom option and a failsafe default.
*/ 

// define variables
private ["_crate","_selection","_side"];
private [
"_rifleMag","_arMag","_mmgMag","_hmgMag",
"_grenade","_smoke",
"_glHE","_glSmokeOne","_glSmokeTwo",
"_lat","_mat",
"_matHE","_matAT",
"_hmgWeapon","_hmgTripod",
"_sniper","_sniperMag"
];

// server check
if (!isServer) exitWith {};

// set variables
_crate = _this select 0;
_selection = toLower(_this select 1);
_side = toLower(_this select 2); 

// default true
if ((time > 1) && (isNil "agent_var_cratePicker_changePerSide")) then { agent_var_cratePicker_changePerSide = true; };

// detects variable from init
if (!agent_var_cratePicker_changePerSide) then { _side = "custom" };

// waits for crate to exist
if (_crate != _crate) then {waitUntil {!isNull _crate}};

// case switch for side relevant cargo
switch (_side) do {
	// BLUFOR CARGO
	case "west" : {
		_rifleMag = "30Rnd_556X45_STANAG";
		_arMag = "200Rnd_556X45_M249";
		_mmgMag = "100Rnd_762X51_M240";
		_hmgMag = "ACE_M2_CSWDM";
		_grenade = "Handgrenade_west";
		_smoke = "SmokeShell";
		_glHE = "1Rnd_HE_M203";
		_glSmokeOne = "1Rnd_SmokeGreen_M203";
		_glSmokeTwo = "1Rnd_SmokeRed_M203";
		_lat = "M136";
		_mat = "MAAWS";
		_matHE = "MAAWS_HEDP";
		_matAT = "MAAWS_HEAT";
		_hmgWeapon = "ACE_M2HBProxy";
		_hmgTripod = "ACE_M3TripodProxy";
		_sniper = "M24";
		_sniperMag = "5Rnd_762x51_M24";
	};
	// OPFOR CARGO
	case "east" : {
		_rifleMag = "30Rnd_545x39_AK";
		_arMag = "75Rnd_545x39_RPK";
		_mmgMag = "100Rnd_762x54_PK";
		_hmgMag = "ACE_KORD_CSWDM";
		_grenade = "Handgrenade_east";
		_smoke = "SmokeShell";
		_glHE = "1Rnd_HE_GP25";
		_glSmokeOne = "1Rnd_SmokeGreen_GP25";
		_glSmokeTwo = "1Rnd_SmokeRed_GP25";
		_lat = "ACE_RPG22";
		_mat = "RPG7V";
		_matHE = "OG7";
		_matAT = "PG7VL";
		_hmgWeapon = "ACE_KORDProxy";
		_hmgTripod = "ACE_6T7TripodProxy";
		_sniper = "SVD";
		_sniperMag = "10Rnd_762x54_SVD";
	};
	// INDFOR CARGO
	case "guer" : {
		_rifleMag = "30Rnd_556X45_STANAG";
		_arMag = "200Rnd_556X45_M249";
		_mmgMag = "100Rnd_762X51_M240";
		_hmgMag = "ACE_M2_CSWDM";
		_grenade = "Handgrenade_west";
		_smoke = "SmokeShell";
		_glHE = "1Rnd_HE_M203";
		_glSmokeOne = "1Rnd_SmokeGreen_M203";
		_glSmokeTwo = "1Rnd_SmokeRed_M203";
		_lat = "M136";
		_mat = "MAAWS";
		_matHE = "MAAWS_HEDP";
		_matAT = "MAAWS_HEAT";
		_hmgWeapon = "ACE_M2HBProxy";
		_hmgTripod = "ACE_M3TripodProxy";
		_sniper = "M24";
		_sniperMag = "5Rnd_762x51_M24";
	};
	// GERMANY CARGO
	case "germany" : {
		_rifleMag = "30Rnd_556x45_G36";
		_arMag = "BWMod_MG4Mag";
		_mmgMag = "BWMod_MG3Mag";
		_hmgMag = "ACE_M2_CSWDM";
		_grenade = "ACE_DM51A1";
		_smoke = "ACE_DM25";
		_glHE = "1Rnd_HE_M203";
		_glSmokeOne = "1Rnd_SmokeGreen_M203";
		_glSmokeTwo = "1Rnd_SmokeRed_M203";
		_lat = "M136";
		_mat = "MAAWS";
		_matHE = "MAAWS_HEDP";
		_matAT = "MAAWS_HEAT";
		_hmgWeapon = "ACE_M2HBProxy";
		_hmgTripod = "ACE_M3TripodProxy";
		_sniper = "M24";
		_sniperMag = "5Rnd_762x51_M24";
	};
	// CUSTOM CARGO - DEFINE CLASSNAMES HERE AFTER SETTING VARIABLE TO FALSE.
	case "custom" : {
		// set custom variables here
		_rifleMag = "30Rnd_556X45_STANAG";
		_arMag = "200Rnd_556X45_M249";
		_mmgMag = "100Rnd_762X51_M240";
		_hmgMag = "ACE_M2_CSWDM";
		_grenade = "Handgrenade_west";
		_smoke = "SmokeShell";
		_glHE = "1Rnd_HE_M203";
		_glSmokeOne = "1Rnd_SmokeGreen_M203";
		_glSmokeTwo = "1Rnd_SmokeRed_M203";
		_lat = "M136";
		_mat = "MAAWS";
		_matHE = "MAAWS_HEDP";
		_matAT = "MAAWS_HEAT";
		_hmgWeapon = "ACE_M2HBProxy";
		_hmgTripod = "ACE_M3TripodProxy";
		_sniper = "M24";
		_sniperMag = "5Rnd_762x51_M24";
	};
	// DEFAULT CARGO, IF NO SIDE GIVEN
	default {
		_rifleMag = "30Rnd_556X45_STANAG";
		_arMag = "200Rnd_556X45_M249";
		_mmgMag = "100Rnd_762X51_M240";
		_hmgMag = "ACE_M2_CSWDM";
		_grenade = "Handgrenade_west";
		_smoke = "SmokeShell";
		_glHE = "1Rnd_HE_M203";
		_glSmokeOne = "1Rnd_SmokeGreen_M203";
		_glSmokeTwo = "1Rnd_SmokeRed_M203";
		_lat = "M136";
		_mat = "MAAWS";
		_matHE = "MAAWS_HEDP";
		_matAT = "MAAWS_HEAT";
		_hmgWeapon = "ACE_M2HBProxy";
		_hmgTripod = "ACE_M3TripodProxy";
		_sniper = "M24";
		_sniperMag = "5Rnd_762x51_M24";
	};
};

// non side specific cargos
_rangefinder = "Binocular_Vector";
_rangefinderBat = "ACE_Battery_Rangefinder";
_designatorBat = "LaserBatteries";
_medOne = "ACE_LargeBandage";
_medTwo = "ACE_Morphine";
_medThree = "ACE_Epinephrine";
_medFour = "ACE_Medkit";
_medFive = "ACE_Tourniquet";

// clear crate
clearMagazineCargoGlobal _crate;
clearWeaponCargoGlobal _crate;

// fill crate depending on desired cargo
switch (_selection) do {

	case "plthq" : {
		_crate addMagazineCargoGlobal [_rifleMag,30];
		_crate addMagazineCargoGlobal [_grenade,8];
		_crate addMagazineCargoGlobal [_smoke,8];
		_crate addMagazineCargoGlobal [_glHE,12];
		_crate addMagazineCargoGlobal [_glSmokeOne,4];
		_crate addMagazineCargoGlobal [_glSmokeTwo,4];
		_crate addMagazineCargoGlobal [_designatorBat,2];
		_crate addMagazineCargoGlobal [_rangefinderBat,2];
		_crate addMagazineCargoGlobal [_medOne,4];
		_crate addMagazineCargoGlobal [_medTwo,2];
	};
	case "section" : {
		_crate addMagazineCargoGlobal [_rifleMag,40];
		_crate addMagazineCargoGlobal [_arMag,4];
		_crate addMagazineCargoGlobal [_grenade,8];
		_crate addMagazineCargoGlobal [_smoke,8];
		_crate addMagazineCargoGlobal [_glHE,12];
		_crate addMagazineCargoGlobal [_glSmokeOne,4];
		_crate addMagazineCargoGlobal [_glSmokeTwo,4];
		_crate addMagazineCargoGlobal [_rangefinderBat,2];
		_crate addMagazineCargoGlobal [_medOne,4];
		_crate addMagazineCargoGlobal [_medTwo,2];
	};
	case "rifle" : {
		_crate addMagazineCargoGlobal [_rifleMag,40];
		_crate addMagazineCargoGlobal [_grenade,8];
		_crate addMagazineCargoGlobal [_smoke,8];
	};
	case "gl" : {
		_crate addMagazineCargoGlobal [_rifleMag,20];
		_crate addMagazineCargoGlobal [_glHE,16];
		_crate addMagazineCargoGlobal [_glSmokeOne,4];
		_crate addMagazineCargoGlobal [_glSmokeTwo,4];
	};
	case "ar" : {
		_crate addMagazineCargoGlobal [_rifleMag,16];
		_crate addMagazineCargoGlobal [_arMag,4];
		_crate addMagazineCargoGlobal [_grenade,4];
		_crate addMagazineCargoGlobal [_smoke,4];
	};
	case "mmg" : {
		_crate addMagazineCargoGlobal [_rifleMag,16];
		_crate addMagazineCargoGlobal [_mmgMag,4];
		_crate addMagazineCargoGlobal [_grenade,4];
		_crate addMagazineCargoGlobal [_smoke,4];
	};
	case "hmg" : {
		_crate addMagazineCargoGlobal [_rifleMag,8];
		_crate addMagazineCargoGlobal [_hmgMag,2];
	};
	case "lat" : {
		_crate addMagazineCargoGlobal [_rifleMag,8];
		_crate addWeaponCargoGlobal [_lat,4];
	};
	case "mat" : {
		_crate addMagazineCargoGlobal [_rifleMag,8];
		_crate addMagazineCargoGlobal [_matAT,4];
		_crate addMagazineCargoGlobal [_matHE,4];
	};
	case "hat" : {
		if (_side == "east") then {
			_crate addMagazineCargoGlobal ["AT13",2];
		} else {
			_crate addWeaponCargoGlobal ["Javelin",2];
		};
		_crate addMagazineCargoGlobal [_rifleMag,8];
	};
	case "explosives" : {
		_crate addMagazineCargoGlobal ["Pipebomb",4];
	};
	case "pltmeds" : {
		_crate addMagazineCargoGlobal [_medOne,12];
		_crate addMagazineCargoGlobal [_medTwo,12];
		_crate addMagazineCargoGlobal [_medThree,12];
		_crate addMagazineCargoGlobal [_medFour,12];
		_crate addMagazineCargoGlobal [_medFive,6];
	};
	case "secmeds" : {
		_crate addMagazineCargoGlobal [_medOne,16];
		_crate addMagazineCargoGlobal [_medTwo,8];
	};
	case "sniperteam" : {
		_crate addWeaponCargoGlobal [_sniper,1];
		_crate addMagazineCargoGlobal [_sniperMag,20];
		_crate addWeaponCargoGlobal [_rangefinder,1];
		_crate addMagazineCargoGlobal [_rangefinderBat,2];
		_crate addWeaponCargoGlobal ["ACE_Kestrel4500",1];
		_crate addWeaponCargoGlobal ["ACE_SpottingScope",1];
	};
	case "matteam" : {
		_crate addWeaponCargoGlobal [_mat,1];
		_crate addMagazineCargoGlobal [_matAT,4];
		_crate addMagazineCargoGlobal [_matHE,4];
		_crate addWeaponCargoGlobal [_rangefinder,1];
		_crate addMagazineCargoGlobal [_rangefinderBat,2];
	};
	case "hatteam" : {
		if (_side == "east") then {
			_crate addMagazineCargoGlobal ["AT13",2];
			_crate addWeaponCargoGlobal ["MetisLauncher",1];
		} else {
			_crate addWeaponCargoGlobal ["Javelin",2];
			_crate addWeaponCargoGlobal ["ACE_Javelin_CLU",1];
		};
		_crate addWeaponCargoGlobal [_rangefinder,1];
		_crate addMagazineCargoGlobal [_rangefinderBat,2];
	};
	case "hmgweapon" : {
		_crate addWeaponCargoGlobal [_hmgWeapon,1];
		_crate addWeaponCargoGlobal [_rangefinder,1];
		_crate addMagazineCargoGlobal [_rangefinderBat,2];
	};
	case "hmgtripod" : {
		_crate addWeaponCargoGlobal [_hmgTripod,1];
	};
};