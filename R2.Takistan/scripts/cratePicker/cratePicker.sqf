#define ACPDLG (uiNamespace getVariable "CratePicker_Dlg")
#define ACPCOMBO (uiNamespace getVariable "CratePicker_Combo")
#define ACPUSEBUTTON (uiNamespace getVariable "CratePicker_UseBtn")

initCratePicker = {
    uiNamespace setVariable ["CratePicker_Dlg", _this select 0];
    uiNamespace setVariable ["CratePicker_Combo", (_this select 0) displayCtrl 1];
    uiNamespace setVariable ["CratePicker_UseBtn", (_this select 0) displayCtrl 3];

    ACPUSEBUTTON ctrlEnable false;

    private "_idx";
    ACPCOMBO lbAdd "--Ammo Supply Crates--";
    _idx = ACPCOMBO lbAdd "PltHQ Supply"; ACPCOMBO lbSetData [_idx, "plthq"];
    _idx = ACPCOMBO lbAdd "Section Supply"; ACPCOMBO lbSetData [_idx, "section"];
	_idx = ACPCOMBO lbAdd "Rifle Ammo"; ACPCOMBO lbSetData [_idx, "rifle"];
    _idx = ACPCOMBO lbAdd "GL Ammo"; ACPCOMBO lbSetData [_idx, "gl"];
    _idx = ACPCOMBO lbAdd "AR Ammo"; ACPCOMBO lbSetData [_idx, "ar"];
    _idx = ACPCOMBO lbAdd "MMG Ammo"; ACPCOMBO lbSetData [_idx, "mmg"];
    _idx = ACPCOMBO lbAdd "HMG Ammo"; ACPCOMBO lbSetData [_idx, "hmg"];
    _idx = ACPCOMBO lbAdd "LAT Ammo"; ACPCOMBO lbSetData [_idx, "lat"];
    _idx = ACPCOMBO lbAdd "MAT Ammo"; ACPCOMBO lbSetData [_idx, "mat"];
    _idx = ACPCOMBO lbAdd "HAT Ammo"; ACPCOMBO lbSetData [_idx, "hat"];
    _idx = ACPCOMBO lbAdd "Explosives"; ACPCOMBO lbSetData [_idx, "explosives"];
    _idx = ACPCOMBO lbAdd "Platoon Meds"; ACPCOMBO lbSetData [_idx, "pltmeds"];
    _idx = ACPCOMBO lbAdd "Section Meds"; ACPCOMBO lbSetData [_idx, "secmeds"];
    ACPCOMBO lbAdd "--Weapons Crates--";
	_idx = ACPCOMBO lbAdd "Sniper Team"; ACPCOMBO lbSetData [_idx, "sniperteam"];
    _idx = ACPCOMBO lbAdd "MAT Team"; ACPCOMBO lbSetData [_idx, "matteam"];
    _idx = ACPCOMBO lbAdd "HAT Team"; ACPCOMBO lbSetData [_idx, "hatteam"];
    _idx = ACPCOMBO lbAdd "HMG Weapon"; ACPCOMBO lbSetData [_idx, "hmgweapon"];
    _idx = ACPCOMBO lbAdd "HMG Tripod"; ACPCOMBO lbSetData [_idx, "hmgtripod"];
};

SpawnSelectedCrate = {
    private ["_idx","_data","_location","_side","_faction"];
    _idx = lbCurSel ACPCOMBO;
    if (_idx < 0) exitWith {};

    _data = ACPCOMBO lbData _idx;
    if (_data == "") exitWith {};

	_location = player modelToWorld [1,0,0];
	
	_side = format ["%1",side player];
	_faction = toLower (faction player); 
	if (_faction == "bis_ger") then { _side = "germany"; };
	
    closeDialog 0;
	[0, {
		private ["_select","_loc","_s","_box"];
		_select = _this select 0;
		_loc = _this select 1;
		_s = _this select 2;
		_box = "ACE_Tbox_US" createVehicle _loc;
		[_box,_select,_s] execVM "scripts\cratePicker\cratePicker_supplyCrates.sqf";
	}, [_data,_location,_side]] call CBA_fnc_globalExecute;
};

CratePicker_OnSelChanged = {
    private "_idx";
    _idx = lbCurSel ACPCOMBO;
    if (_idx < 0) exitWith
    {
        ACPUSEBUTTON ctrlEnable false;
    };

    _data = ACPCOMBO lbData _idx;
    if (_data == "") exitWith
    {
        ACPUSEBUTTON ctrlEnable false;
    };
    ACPUSEBUTTON ctrlEnable true;
};
