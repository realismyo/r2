/* 
Gear Assign Script
by AGeNT

- Covers most standard RIFM platoon roles, if you want to add more just ask me or do so by observation of how the others work. It shouldn't be too hard to figure out :)
- Current loadout faction: KSK OA
- Current Loadouts: pltld, pltmed, pltfac, secco, sectl, ar, aar, rm, rmat, dmr, gren, mmg, mmgass, pilot, crewman, hmggun, hmgass, hmgammo, matgun, matammo, hatgun, hatammo

=== Using the Scripts ===
- If called from unit init field, needs to look similar to below, with the desired loadout in ""
- nul = [this,"DesiredLoadoutHere"] execVM "scripts\assignGear_KSK.sqf";
e.g. - nul = [this,"pltld"] execVM "scripts\assignGear_KSK.sqf";

- Include 'ace_sys_wounds_no_medical_gear = true;' without quotes in your init.sqf
- Classnames can be edited below for whatever weapons you'd like units to have.
- Additional cases can be added below to support additional roles.
- If you're creating a role that requires a weapon on back, pay attention to the rmat role. 
====================
*/ 

// define variables - seperated for easier addition/removal of variables
private [
"_delay","_unit","_loadout",
"_rifle","_rifleGL","_rifleScoped","_rifleGLScoped","_autoRifle","_carbine","_mmg","_smg","_pistol","_gl",
"_grenade","_smoke","_throwG",
"_rifleMag","_rifleGLMag","_rifleTracerMag","_rifleGLMag","_rifleScopedMag","_autoRifleMag","_carbineMag","_mmgMag","_smgMag","_pistolMag",
"_glExplody","_glSmokeOne","_glSmokeTwo","_glSmokeThree",
"_medRuck","_plebRuck","_radioRuck",
"_medOne","_medTwo","_medThree","_medFour","_medFive","_medSix",
"_IFAK","_RIFM_IFAK",
"_lat","_binos","_radio",
"_mat","_matAT","_matHE","_matMags","_hatTool","_hatMag",
"_rangeFinder","_rangeFinderBat","_designator","_designatorBat",
"_facRuck","_facTools",
"_hmgBarrel","_hmgTripod","_hmgMag",
"_basicTools","_secTools","_pltTools","_autoTools",
"_wob"
];

_delay = 0.5;
_unit = _this select 0;
_loadout = toLower (_this select 1);

// ===== EDIT CLASSNAMES HERE =====
// general weapons
_rifle = "ACE_HK416_D14_COMPM3";
_rifleGL = "ACE_HK416_D14_COMPM3_M320";
_rifleScoped = "ACE_G36A2";
_autoRifle = "BWMod_MG4";
_carbine = "ACE_HK416_D10";
_mmg = "BWMod_MG3";
_smg = "ACE_KAC_PDW";
_pistol = "ACE_P8";
// general throwables
_grenade = "ACE_DM51A1"; 
_smoke = "ACE_DM25";
_throwG = [_smoke,_grenade];
// general magazines
_rifleMag = "30Rnd_556x45_STANAG";
_rifleTracerMag = "ACE_30Rnd_556x45_T_STANAG";
_rifleGLMag = "30Rnd_556x45_STANAG";
_rifleScopedMag = "30Rnd_556x45_G36";
_autoRifleMag = "BWMod_MG4Mag";
_carbineMag = "30Rnd_556x45_STANAG";
_mmgMag = "BWMod_MG3Mag";
_smgMag = "ACE_30Rnd_6x35_B_PDW";
_pistolMag = "ACE_15Rnd_9x19_P8";
// gl rounds
_glExplody = "1Rnd_HE_M203";
_glSmokeOne = "1Rnd_SmokeGreen_M203";
_glSmokeTwo = "1Rnd_SmokeRed_M203";
_glSmokeThree = "1Rnd_Smoke_M203";
// rucks
_medRuck = "ACE_VTAC_RUSH72_TT_Medic";
_plebRuck = "ACE_Backpack_Multicam";
_radioRuck = "ACRE_PRC117F";
// medical items
_medOne = "ACE_Bandage";
_medTwo = "ACE_Morphine";
_medThree = "ACE_Epinephrine";
_medFour = "ACE_LargeBandage";
_medFive = "ACE_Medkit";
_medSix = "ACE_Tourniquet";
_IFAK = [_medFour,_medFour,_medTwo];
_RIFM_IFAK = { {[_unit,_x,1] call ACE_fnc_PackMagazine} foreach _IFAK};		// any extra meds required (bandages, morphine)
// -- specialist items --
_lat = "M136";
_binos = "Binocular";
_radio = "ACRE_PRC148";
// anti-tank
_mat = "MAAWS";
_matAT = "MAAWS_HEAT";
_matHE = "MAAWS_HEDP";
_matMags = [_matAT,_matHE];
_hatTool = "ACE_Javelin_CLU";
_hatMag = "Javelin";
// rangefinders
_rangeFinder = "Binocular_Vector";
_rangeFinderBat = "ACE_Battery_Rangefinder";
// laser des
_designator = "LaserDesignator";
_designatorBat = "LaserBatteries";
// fac
_facRuck = "ACRE_PRC117F";
_facTools = [_designator,"ACE_DAGR","itemGPS","ACE_Map_Tools"];
// hmg team
_hmgBarrel = "ACE_M2HBProxy";
_hmgTripod = "ACE_M3TripodProxy";
_hmgMag = "ACE_M2_CSWDM";
// tools
_basicTools = ["ACRE_PRC343","ItemCompass","ACE_Map","ItemWatch","ACE_Earplugs","ACE_KeyCuffs","NVGoggles"];
_secTools = [_rangeFinder,"itemGPS"];
_pltTools = [_rangeFinder,"itemGPS","ACE_Map_Tools"];
_autoTools = [_binos,"ACE_SpareBarrel"];
// ==================================================
waitUntil {time > 1};
if (_unit != _unit) then {waitUntil {!isNull _unit}};	// make sure unit exists, or wait until it does
if !(isPlayer _unit) then {doStop _unit};
[_unit, "BTH"] call ACE_fnc_RemoveGear; 				// remove weapon on back & rucksack gear.
removeAllItems _unit;
removeAllWeapons _unit; 
removeBackpack _unit;

sleep _delay;
{ _unit addWeapon _x } foreach _basicTools; 			// add each of the basic tools
[_unit, -1, -1, -1, true] call ACE_fnc_PackIFAK; 		// IFAK
sleep _delay;

switch (_loadout) do {									// case switch for desired loadout
	
	// -- Platoon HQ Roles --
	// plt leader gear, doubles as plt sgt
	case "pltld" : {
		_unit addWeapon _radioRuck;
		{ _unit addWeapon _x } foreach _pltTools;
		for "_i" from 1 to 7 do {_unit addMagazine _rifleGLMag};
		_unit addMagazine _rangeFinderBat;
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 4 do {_unit addMagazine _glExplody};
		for "_i" from 1 to 2 do {_unit addMagazine _glSmokeOne};
		for "_i" from 1 to 2 do {_unit addMagazine _glSmokeTwo};
		_unit addWeapon _rifleGL;
		[_unit,_rifleMag,2] call ACE_fnc_PackMagazine;
		[_unit,_rifleTracerMag,2] call ACE_fnc_PackMagazine;
		call _RIFM_IFAK;
		[_unit,_rangeFinderBat, 1] call ACE_fnc_PackMagazine;
		[_unit,_glExplody,6] call ACE_fnc_PackMagazine;
		[_unit,_glSmokeOne,1] call ACE_fnc_PackMagazine;
		[_unit,_glSmokeTwo,1] call ACE_fnc_PackMagazine;
	};
	// Plt Medic
	case "pltmed" : {
		_unit addWeapon _medRuck;
		for "_i" from 1 to 8 do {_unit addMagazine _rifleMag};
		for "_i" from 1 to 4 do {_unit addMagazine _smoke};
		for "_i" from 1 to 5 do {_unit addMagazine _medSix};
		_unit addWeapon _rifle;
		{[_unit,_x,16] call ACE_fnc_PackMagazine} foreach [_medThree,_medFour];
		[_unit,_medTwo,12] call ACE_fnc_PackMagazine;
		[_unit,_medFive,14] call ACE_fnc_PackMagazine;
		_unit setVariable ["ace_w_ismedic",true]; 
	};
	// Plt FAC/FO
	case "pltfac" : {
		_unit addWeapon _facRuck;
		{ _unit addWeapon _x } foreach _facTools;
		for "_i" from 1 to 7 do {_unit addMagazine _rifleMag};
		_unit addMagazine _designatorBat;
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 3 do {_unit addMagazine _pistolMag};
		_unit addWeapon _rifle;
		_unit addWeapon _pistol;
		[_unit,_rifleMag,4] call ACE_fnc_PackMagazine;
		call _RIFM_IFAK;
		[_unit,_designatorBat,1] call ACE_fnc_PackMagazine;
	};
	
	// -- Section Roles --
	// section leader
	case "secco" : {
		_unit addWeapon _radioRuck;
		{ _unit addWeapon _x } foreach _secTools;
		for "_i" from 1 to 7 do {_unit addMagazine _rifleGLMag};
		_unit addMagazine _rangeFinderBat;
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 4 do {_unit addMagazine _glExplody};
		for "_i" from 1 to 2 do {_unit addMagazine _glSmokeOne};
		for "_i" from 1 to 2 do {_unit addMagazine _glSmokeTwo};
		_unit addWeapon _rifleGL;
		[_unit,_rifleMag,2] call ACE_fnc_PackMagazine;
		[_unit,_rifleTracerMag,2] call ACE_fnc_PackMagazine;
		call _RIFM_IFAK;
		[_unit,_rangeFinderBat, 1] call ACE_fnc_PackMagazine;
		[_unit,_glExplody,6] call ACE_fnc_PackMagazine;
		[_unit,_glSmokeOne,1] call ACE_fnc_PackMagazine;
		[_unit,_glSmokeTwo,1] call ACE_fnc_PackMagazine;
	};
	// Section Team Leader / 2iC
	case "sectl" : {
		_unit addWeapon _plebRuck;
		{ _unit addWeapon _x } foreach _secTools;
		for "_i" from 1 to 7 do {_unit addMagazine _rifleGLMag};
		_unit addMagazine _rangeFinderBat;
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 4 do {_unit addMagazine _glExplody};
		for "_i" from 1 to 2 do {_unit addMagazine _glSmokeOne};
		for "_i" from 1 to 2 do {_unit addMagazine _glSmokeTwo};
 		_unit addWeapon _rifleGL;
		[_unit,_rifleGLMag,8] call ACE_fnc_PackMagazine;
		{[_unit,_x,2] call ACE_fnc_PackMagazine} foreach _throwG;
		call _RIFM_IFAK;
		[_unit,_rangeFinderBat, 1] call ACE_fnc_PackMagazine;
		[_unit,_glExplody,14] call ACE_fnc_PackMagazine;
		[_unit,_glSmokeOne,3] call ACE_fnc_PackMagazine;
		[_unit,_glSmokeTwo,3] call ACE_fnc_PackMagazine;	
		[_unit,_autoRifleMag,1] call ACE_fnc_PackMagazine;
	};
	// Automatic Rifleman
	case "ar" : {
		_unit addWeapon _plebRuck;
		_unit addWeapon (_autoTools select 1);
		for "_i" from 1 to 4 do {_unit addMagazine _autoRifleMag};
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		_unit addWeapon _autoRifle;
		call _RIFM_IFAK;
	};
	// Assistant Automatic Rifleman
	case "aar" : {
		_unit addWeapon _plebRuck;
		_unit addWeapon (_autoTools select 0);
		for "_i" from 1 to 8 do {_unit addMagazine _rifleMag};
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 3 do {_unit addMagazine _pistolMag};
		_unit addWeapon _rifle;
		_unit addWeapon _pistol;
		[_unit,_rifleMag,4] call ACE_fnc_PackMagazine;
		[_unit,_autoRifleMag,4] call ACE_fnc_PackMagazine;
		{[_unit,_x,2] call ACE_fnc_PackMagazine} foreach _throwG;
		call _RIFM_IFAK;
	};
	// Rifleman
	case "rm" : {
		_unit addWeapon _plebRuck;
		for "_i" from 1 to 8 do {_unit addMagazine _rifleMag};
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 3 do {_unit addMagazine _pistolMag};
		_unit addWeapon _rifle;
		_unit addWeapon _pistol;
		[_unit,_rifleMag,8] call ACE_fnc_PackMagazine;
		{[_unit,_x,2] call ACE_fnc_PackMagazine} foreach _throwG;
		call _RIFM_IFAK;
		[_unit,_autoRifleMag,2] call ACE_fnc_PackMagazine;
	};	
	// Rifleman (disposable light AT)
	case "rmat" : {
		if (player == _unit) then {
			_unit addWeapon _lat;
			waitUntil {_wob = [_unit, _lat] call ACE_fnc_PutWeaponOnBack; _wob};
			_unit addWeapon _plebRuck;
		};
		for "_i" from 1 to 8 do {_unit addMagazine _rifleMag};
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		_unit addWeapon _rifle;
		[_unit,_rifleMag,8] call ACE_fnc_PackMagazine;
		{[_unit,_x,2] call ACE_fnc_PackMagazine} foreach _throwG;
		call _RIFM_IFAK;
		[_unit,_autoRifleMag,1] call ACE_fnc_PackMagazine;
	};
	// Designated Marksman / Scoped Rifleman
	case "dmr" : {
		_unit addWeapon _plebRuck;
		for "_i" from 1 to 8 do {_unit addMagazine _rifleScopedMag};
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 3 do {_unit addMagazine _pistolMag};
		_unit addWeapon _rifleScoped;
		_unit addWeapon _pistol;
		[_unit,_rifleScopedMag,8] call ACE_fnc_PackMagazine;
		{[_unit,_x,2] call ACE_fnc_PackMagazine} foreach _throwG;
		call _RIFM_IFAK;
		[_unit,_autoRifleMag,2] call ACE_fnc_PackMagazine;
	};
	// Grenadier
	case "gren" : {
		_unit addWeapon _plebRuck;
		for "_i" from 1 to 8 do {_unit addMagazine _rifleGLMag};
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 4 do {_unit addMagazine _glExplody};
		for "_i" from 1 to 2 do {_unit addMagazine _glSmokeOne};
		for "_i" from 1 to 2 do {_unit addMagazine _glSmokeTwo};
 		_unit addWeapon _rifleGL;
		[_unit,_rifleGLMag,8] call ACE_fnc_PackMagazine;
		{[_unit,_x,2] call ACE_fnc_PackMagazine} foreach _throwG;
		[_unit,_glExplody,14] call ACE_fnc_PackMagazine;
		[_unit,_glSmokeOne,3] call ACE_fnc_PackMagazine;
		[_unit,_glSmokeTwo,3] call ACE_fnc_PackMagazine;		
		call _RIFM_IFAK;
		[_unit,_autoRifleMag,1] call ACE_fnc_PackMagazine;
	};
	// Machinegunner
	case "mmg" : {
		_unit addWeapon (_autoTools select 1);
		for "_i" from 1 to 4 do {_unit addMagazine _mmgMag};
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		_unit addWeapon _mmg;
		{ _unit addMagazine _x } foreach _IFAK;
	};
	// Assistant Machinegunner
	case "mmgass" : {
		_unit addWeapon _plebRuck;
		_unit addWeapon (_autoTools select 0);
		for "_i" from 1 to 8 do {_unit addMagazine _rifleMag};
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 3 do {_unit addMagazine _pistolMag};
		_unit addWeapon _rifle;
		_unit addWeapon _pistol;
		[_unit,_rifleMag,4] call ACE_fnc_PackMagazine;
		[_unit,_mmgMag,4] call ACE_fnc_PackMagazine;
		call _RIFM_IFAK;
	};
	
	// -- Vehicle Crews --
	// pilot
	case "pilot" : {
		_unit addWeapon _radio;
		_unit addWeapon _radioRuck;
		for "_i" from 1 to 8 do {_unit addMagazine _smgMag};
		for "_i" from 1 to 4 do {_unit addMagazine _smoke};
		for "_i" from 1 to 3 do {_unit addMagazine _pistolMag};
		_unit addWeapon _smg;
		_unit addWeapon _pistol;
		{ _unit addMagazine _x } foreach _IFAK;
		[_unit, true] call ACE_fnc_setCrewProtection;
	};
	// crewmans
	case "crewman" : {
		_unit addWeapon _radio;
		for "_i" from 1 to 8 do {_unit addMagazine _carbineMag};
		for "_i" from 1 to 4 do {_unit addMagazine _smoke};
		for "_i" from 1 to 3 do {_unit addMagazine _pistolMag};
		_unit addWeapon _carbine;
		_unit addWeapon _pistol;
		{ _unit addMagazine _x } foreach _IFAK;
		[_unit, true] call ACE_fnc_setCrewProtection;
	};
	
	// -- Weapons Teams --
	// HMG Gunner
	case "hmggun" : {
		_unit addWeapon _hmgBarrel;
		for "_i" from 1 to 8 do {_unit addMagazine _rifleMag};
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		_unit addWeapon _rifle;
		{ _unit addMagazine _x } foreach _IFAK;
	};
	// HMG Assistant
	case "hmgass" : {
		_unit addWeapon _hmgTripod;
		_unit addWeapon _rangeFinder;
		for "_i" from 1 to 7 do {_unit addMagazine _rifleMag};
		for "_i" from 1 to 2 do {_unit addMagazine _smoke};
		for "_i" from 1 to 1 do {_unit addMagazine _hmgMag};
		_unit addMagazine _rangeFinderBat;
		_unit addWeapon _rifle;
		{ _unit addMagazine _x } foreach _IFAK;
	};
	// HMG Ammobearer
	case "hmgammo" : {
		_unit addWeapon _plebRuck;
		_unit addWeapon _binos;
		for "_i" from 1 to 8 do {_unit addMagazine _rifleMag};
		for "_i" from 1 to 2 do {_unit addMagazine _hmgMag};
		_unit addWeapon _rifle;
		call _RIFM_IFAK;
	};
	// MAT Gunner
	case "matgun" : {
		_unit addWeapon _mat;
		for "_i" from 1 to 6 do {_unit addMagazine _rifleMag};
		{ _unit addMagazine _x } foreach _matMags;
		for "_i" from 1 to 3 do {_unit addMagazine _pistolMag};
		_unit addWeapon _rifle;
		_unit addWeapon _pistol;
		{ _unit addMagazine _x } foreach _IFAK;
	};
	// MAT Assistant
	case "matammo" : {
		_unit addWeapon _plebRuck;
		_unit addWeapon _rangeFinder;
		for "_i" from 1 to 5 do {_unit addMagazine _rifleMag};
		{ _unit addMagazine _x } foreach _matMags;
		_unit addMagazine _rangeFinderBat;
		for "_i" from 1 to 3 do {_unit addMagazine _pistolMag};
		_unit addWeapon _rifle;
		_unit addWeapon _pistol;
		{ _unit addMagazine _x } foreach _IFAK;
		{ [_unit,_x,1] call ACE_fnc_PackMagazine } foreach _matMags;
		[_unit,_rifleMag,6] call ACE_fnc_PackMagazine;
		[_unit,_rangeFinderBat,1] call ACE_fnc_PackMagazine;
	};
	// HAT Gunner
	case "hatgun" : {
		_unit addWeapon _hatTool;
		_unit addWeapon _hatMag;
		for "_i" from 1 to 8 do {_unit addMagazine _rifleMag};
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 3 do {_unit addMagazine _pistolMag};
		_unit addWeapon _rifle;
		_unit addWeapon _pistol;
		{ _unit addMagazine _x } foreach _IFAK;
	};
	// HAT Assistant
	case "hatammo" : {
		_unit addWeapon _rangeFinder;
		_unit addWeapon _hatMag;
		for "_i" from 1 to 7 do {_unit addMagazine _rifleMag};
		_unit addMagazine _rangeFinderBat;
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 3 do {_unit addMagazine _pistolMag};
		_unit addWeapon _rifle;
		_unit addWeapon _pistol;
		{ _unit addMagazine _x } foreach _IFAK;
	};
	// if undefined or incorrectly defined, give hint and assign standard rifleman gear
	default {
		_unit sideChat format ["No or incorrectly defined loadout for %1",_unit];
		_unit addWeapon _plebRuck;
		for "_i" from 1 to 8 do {_unit addMagazine _rifleMag};
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 3 do {_unit addMagazine _pistolMag};
		_unit addWeapon _rifle;
		_unit addWeapon _pistol;
		[_unit,_rifleMag,8] call ACE_fnc_PackMagazine;
		{[_unit,_x,2] call ACE_fnc_PackMagazine} foreach _throwG;
		call _RIFM_IFAK;
		[_unit,_autoRifleMag,2] call ACE_fnc_PackMagazine;
	};
};

sleep _delay;
_unit GroupChat format ["%1 completed gear assign", name _unit];