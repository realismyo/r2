/* 
Gear Assign Script
by AGeNT

- Covers most standard RIFM platoon roles, if you want to add more just ask me or do so by observation of how the others work. It shouldn't be too hard to figure out :)
- Current loadout faction: Takistani Army
- Current Loadouts: pltld, pltmed, pltfac, secco, sectl, ar, aar, rm, rmat, dmr, gren, mmg, mmgass, pilot, crewman, hmggun, hmgass, hmgammo, matgun, matammo, hatgun, hatammo

=== Using the Scripts ===
- If called from unit init field, needs to look similar to below, with the desired loadout in ""
- nul = [this,"DesiredLoadoutHere"] execVM "scripts\assignGear_TakiArmy.sqf";
e.g. - nul = [this,"pltld"] execVM "scripts\assignGear_TakiArmy.sqf";

- Include 'ace_sys_wounds_no_medical_gear = true;' without quotes in your init.sqf
- Classnames can be edited below for whatever weapons you'd like units to have.
- Additional cases can be added below to support additional roles.
- If you're creating a role that requires a weapon on back, pay attention to the rmat role. 
====================
*/ 

// define variables - seperated for easier addition/removal of variables
private [
"_delay","_unit","_loadout",
"_rifle","_rifleGL","_rifleScoped","_autoRifle","_carbine","_mmg","_smg","_pistol",
"_grenade","_smoke","_throwG",
"_rifleMag","_rifleTracerMag","_rifleGLMag","_rifleScopedMag","_autoRifleMag","_carbineMag","_mmgMag","_smgMag","_pistolMag",
"_glExplody","_glSmokeOne","_glSmokeTwo","_glSmokeThree",
"_medRuck","_plebRuck","_radioRuck",
"_medOne","_medTwo","_medThree","_medFour","_medFive","_medSix",
"_IFAK","_RIFM_IFAK",
"_lat","_rpg","_rpgHE","_rpgAT","_rpgTAND","_rpgMags","_binos","_radio",
"_hat","_hatMag",
"_rangeFinder","_rangeFinderBat","_designator","_designatorBat",
"_facRuck","_facTools",
"_hmgBarrel","_hmgTripod","_hmgMag",
"_basicTools","_secTools","_pltTools","_autoTools",
"_wob"
];

_delay = 0.5;
_unit = _this select 0;
_loadout = toLower (_this select 1);

// ===== EDIT CLASSNAMES HERE =====
// general weapons
_rifle = "AKS_74";
_rifleGL = "ACE_AKS74_GP25";
_rifleScoped = "AKS_74_pso";
_autoRifle = "ACE_RPK74M";
_carbine = "AKS_74";
_mmg = "PK";
_smg = "AKS_74_U";
_pistol = "Makarov";
// general throwables
_grenade = "Handgrenade_east"; 
_smoke = "SmokeShell";
_throwG = [_grenade,_smoke];
// general magazines
_rifleMag = "30Rnd_545x39_AK";
_rifleTracerMag = "ACE_30Rnd_545x39_T_AK";
_rifleGLMag = "30Rnd_545x39_AK";
_rifleScopedMag = "30Rnd_545x39_AK";
_autoRifleMag = "75Rnd_545x39_RPK";
_carbineMag = "30Rnd_545x39_AK";
_mmgMag = "100Rnd_762x54_PK";
_smgMag = "30Rnd_545x39_AK";
_pistolMag = "8Rnd_9x18_Makarov";
// gl rounds
_glExplody = "1Rnd_HE_GP25";
_glSmokeOne = "1Rnd_SmokeGreen_GP25";
_glSmokeTwo = "1Rnd_SmokeRed_GP25";
_glSmokeThree = "1Rnd_Smoke_GP25";
// rucks
_medRuck = "ACE_Rucksack_Molle_Green_Medic";
_plebRuck = "ACE_Rucksack_Molle_Green";
_radioRuck = "ACRE_PRC117F";
// medical items
_medOne = "ACE_Bandage";
_medTwo = "ACE_Morphine";
_medThree = "ACE_Epinephrine";
_medFour = "ACE_LargeBandage";
_medFive = "ACE_Medkit";
_medSix = "ACE_Tourniquet";
_IFAK = [_medFour,_medFour,_medTwo];
_RIFM_IFAK = { {[_unit,_x,1] call ACE_fnc_PackMagazine} foreach _IFAK};		// any extra meds required (bandages, morphine)
// -- specialist items --
_lat = "ACE_RPG22";
_rpg = "RPG7V";
_rpgHE = "OG7";
_rpgAT = "PG7VL";
_rpgTAND = "PG7VR";
_rpgMags = [_rpgHE,_rpgAT];
_binos = "Binocular";
_radio = "ACRE_PRC148";
// anti-tank
_hat = "MetisLauncher";
_hatMag = "AT13";
// rangefinders
_rangeFinder = "Binocular_Vector";
_rangeFinderBat = "ACE_Battery_Rangefinder";
// laser des
_designator = "LaserDesignator";
_designatorBat = "LaserBatteries";
// fac
_facRuck = "ACRE_PRC117F";
_facTools = [_designator,"ACE_DAGR","itemGPS","ACE_Map_Tools"];
// hmg team
_hmgBarrel = "ACE_KORDProxy";
_hmgTripod = "ACE_6T7TripodProxy";
_hmgMag = "ACE_KORD_CSWDM";
// tools
_basicTools = ["ACRE_PRC343","ItemCompass","ACE_Map","ItemWatch","ACE_Earplugs","ACE_KeyCuffs","NVGoggles"];
_secTools = [_rangeFinder,"itemGPS"];
_pltTools = [_rangeFinder,"itemGPS","ACE_Map_Tools"];
_autoTools = [_binos,"ACE_SpareBarrel"];
// ==================================================
waitUntil {time > 1};
if (_unit != _unit) then {waitUntil {!isNull _unit}};	// make sure unit exists, or wait until it does
if !(isPlayer _unit) then {doStop _unit};				// remove this line if arming AI
[_unit, "BTH"] call ACE_fnc_RemoveGear; 				// remove weapon on back & rucksack gear.
removeAllItems _unit;
removeAllWeapons _unit; 
removeBackpack _unit;

sleep _delay;
{ _unit addWeapon _x } foreach _basicTools; 			// add each of the basic tools
[_unit, -1, -1, -1, true] call ACE_fnc_PackIFAK; 		// IFAK
sleep _delay;

switch (_loadout) do {									// case switch for desired loadout

	// -- Platoon HQ Roles --
	// Plt Commander gear, doubles as Plt Sgt
	case "pltld" : {
		_unit addWeapon _radioRuck;
		{ _unit addWeapon _x } foreach _pltTools;
		for "_i" from 1 to 5 do {_unit addMagazine _rifleMag};
		for "_i" from 1 to 2 do {_unit addMagazine _rifleTracerMag};
		_unit addMagazine _rangeFinderBat;
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 4 do {_unit addMagazine _glExplody};
		for "_i" from 1 to 2 do {_unit addMagazine _glSmokeOne};
		for "_i" from 1 to 2 do {_unit addMagazine _glSmokeTwo};
		_unit addWeapon _rifleGL;
		[_unit,_rifleMag,2] call ACE_fnc_PackMagazine;
		[_unit,_rifleTracerMag,2] call ACE_fnc_PackMagazine;
		call _RIFM_IFAK;
		[_unit,_rangeFinderBat, 1] call ACE_fnc_PackMagazine;
		[_unit,_glExplody,4] call ACE_fnc_PackMagazine;
		[_unit,_glSmokeOne,1] call ACE_fnc_PackMagazine;
		[_unit,_glSmokeTwo,1] call ACE_fnc_PackMagazine;
	};
	// Plt Medic
	case "pltmed" : {
		_unit addWeapon _medRuck;
		for "_i" from 1 to 8 do {_unit addMagazine _rifleMag};
		for "_i" from 1 to 4 do {_unit addMagazine _smoke};
		for "_i" from 1 to 5 do {_unit addMagazine _medSix};
		_unit addWeapon _rifle;
		{[_unit,_x,16] call ACE_fnc_PackMagazine} foreach [_medThree,_medFour];
		[_unit,_medTwo,12] call ACE_fnc_PackMagazine;
		[_unit,_medFive,14] call ACE_fnc_PackMagazine;
		_unit setVariable ["ace_w_ismedic",true]; 
	};
	// Plt FAC/FO
	case "pltfac" : {
		_unit addWeapon _facRuck;
		{ _unit addWeapon _x } foreach _facTools;
		for "_i" from 1 to 7 do {_unit addMagazine _rifleMag};
		_unit addMagazine _designatorBat;
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 3 do {_unit addMagazine _pistolMag};
		_unit addWeapon _rifle;
		_unit addWeapon _pistol;
		[_unit,_rifleMag,4] call ACE_fnc_PackMagazine;
		call _RIFM_IFAK;
		[_unit,_designatorBat,1] call ACE_fnc_PackMagazine;
	};
	
	// -- Section Roles --
	// Section Commander
	case "secco" : {
		_unit addWeapon _radioRuck;
		{ _unit addWeapon _x } foreach _secTools;
		for "_i" from 1 to 5 do {_unit addMagazine _rifleMag};
		for "_i" from 1 to 2 do {_unit addMagazine _rifleTracerMag};
		_unit addMagazine _rangeFinderBat;
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 4 do {_unit addMagazine _glExplody};
		for "_i" from 1 to 2 do {_unit addMagazine _glSmokeOne};
		for "_i" from 1 to 2 do {_unit addMagazine _glSmokeTwo};
		_unit addWeapon _rifleGL;
		[_unit,_rifleMag,2] call ACE_fnc_PackMagazine;
		[_unit,_rifleTracerMag,2] call ACE_fnc_PackMagazine;
		call _RIFM_IFAK;
		[_unit,_rangeFinderBat, 1] call ACE_fnc_PackMagazine;
		[_unit,_glExplody,4] call ACE_fnc_PackMagazine;
		[_unit,_glSmokeOne,1] call ACE_fnc_PackMagazine;
		[_unit,_glSmokeTwo,1] call ACE_fnc_PackMagazine;
	};
	// Section Team Leader / 2iC
	case "sectl" : {
		_unit addWeapon _plebRuck;
		{ _unit addWeapon _x } foreach _secTools;
		for "_i" from 1 to 5 do {_unit addMagazine _rifleGLMag};
		for "_i" from 1 to 2 do {_unit addMagazine _rifleTracerMag};
		_unit addMagazine _rangeFinderBat;
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 4 do {_unit addMagazine _glExplody};
		for "_i" from 1 to 2 do {_unit addMagazine _glSmokeOne};
		for "_i" from 1 to 2 do {_unit addMagazine _glSmokeTwo};
 		_unit addWeapon _rifleGL;
		[_unit,_rifleGLMag,8] call ACE_fnc_PackMagazine;
		{[_unit,_x,2] call ACE_fnc_PackMagazine} foreach _throwG;
		[_unit,_glExplody,14] call ACE_fnc_PackMagazine;
		[_unit,_glSmokeOne,3] call ACE_fnc_PackMagazine;
		[_unit,_glSmokeTwo,3] call ACE_fnc_PackMagazine;	
		call _RIFM_IFAK;
		[_unit,_rangeFinderBat, 1] call ACE_fnc_PackMagazine;
		[_unit,_autoRifleMag,1] call ACE_fnc_PackMagazine;
	};
	// Automatic Rifleman
	case "ar" : {
		_unit addWeapon _plebRuck;
		_unit addWeapon (_autoTools select 1);
		for "_i" from 1 to 8 do {_unit addMagazine _autoRifleMag};
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		_unit addWeapon _autoRifle;
		[_unit,_autoRifleMag,8] call ACE_fnc_PackMagazine;
		{[_unit,_x,2] call ACE_fnc_PackMagazine} foreach _throwG;
		call _RIFM_IFAK;
	};
	// Assistant Automatic Rifleman
	case "aar" : {
		_unit addWeapon _plebRuck;
		_unit addWeapon (_autoTools select 0);
		for "_i" from 1 to 8 do {_unit addMagazine _rifleMag};
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 3 do {_unit addMagazine _pistolMag};
		_unit addWeapon _rifle;
		_unit addWeapon _pistol;
		[_unit,_autoRifleMag,8] call ACE_fnc_PackMagazine;
		[_unit,_rifleMag,4] call ACE_fnc_PackMagazine;
		{[_unit,_x,2] call ACE_fnc_PackMagazine} foreach _throwG;
		call _RIFM_IFAK;
	};
	// Rifleman
	case "rm" : {
		_unit addWeapon _plebRuck;
		for "_i" from 1 to 8 do {_unit addMagazine _rifleMag};
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 3 do {_unit addMagazine _pistolMag};
		_unit addWeapon _rifle;
		_unit addWeapon _pistol;
		[_unit,_rifleMag,8] call ACE_fnc_PackMagazine;
		{[_unit,_x,2] call ACE_fnc_PackMagazine} foreach _throwG;
		call _RIFM_IFAK;
		[_unit,_autoRifleMag,2] call ACE_fnc_PackMagazine;
	};	
	// Rifleman
	case "rmat" : {
		if (player == _unit) then {
			_unit addWeapon _lat;
			waitUntil {_wob = [_unit, _lat] call ACE_fnc_PutWeaponOnBack; _wob};
			_unit addWeapon _plebRuck;
		};
		for "_i" from 1 to 8 do {_unit addMagazine _rifleMag};
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 3 do {_unit addMagazine _pistolMag};
		_unit addWeapon _rifle;
		_unit addWeapon _pistol;
		[_unit,_rifleMag,8] call ACE_fnc_PackMagazine;
		{[_unit,_x,2] call ACE_fnc_PackMagazine} foreach _throwG;
		call _RIFM_IFAK;
		[_unit,_autoRifleMag,2] call ACE_fnc_PackMagazine;
	};	
	// Designated Marksman / Scoped Rifleman
	case "dmr" : {
		_unit addWeapon _plebRuck;
		for "_i" from 1 to 8 do {_unit addMagazine _rifleScopedMag};
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 3 do {_unit addMagazine _pistolMag};
		_unit addWeapon _rifleScoped;
		_unit addWeapon _pistol;
		[_unit,_rifleScopedMag,8] call ACE_fnc_PackMagazine;
		{[_unit,_x,2] call ACE_fnc_PackMagazine} foreach _throwG;
		call _RIFM_IFAK;
		[_unit,_autoRifleMag,2] call ACE_fnc_PackMagazine;
	};
	// Grenadier
	case "gren" : {
		_unit addWeapon _plebRuck;
		for "_i" from 1 to 8 do {_unit addMagazine _rifleGLMag};
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 4 do {_unit addMagazine _glExplody};
		for "_i" from 1 to 2 do {_unit addMagazine _glSmokeOne};
		for "_i" from 1 to 2 do {_unit addMagazine _glSmokeTwo};
 		_unit addWeapon _rifleGL;
		[_unit,_rifleGLMag,8] call ACE_fnc_PackMagazine;
		{[_unit,_x,2] call ACE_fnc_PackMagazine} foreach _throwG;
		[_unit,_glExplody,14] call ACE_fnc_PackMagazine;
		[_unit,_glSmokeOne,3] call ACE_fnc_PackMagazine;
		[_unit,_glSmokeTwo,3] call ACE_fnc_PackMagazine;	
		call _RIFM_IFAK;
		[_unit,_autoRifleMag,1] call ACE_fnc_PackMagazine;
	};
	// Machinegunner
	case "mmg" : {
		_unit addWeapon (_autoTools select 1);
		for "_i" from 1 to 4 do {_unit addMagazine _mmgMag};
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		_unit addWeapon _mmg;
		{ _unit addMagazine _x } foreach _IFAK;
	};
	// Assistant Machinegunner
	case "mmgass" : {
		_unit addWeapon _plebRuck;
		_unit addWeapon (_autoTools select 0);
		for "_i" from 1 to 8 do {_unit addMagazine _rifleMag};
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 3 do {_unit addMagazine _pistolMag};
		_unit addWeapon _rifle;
		_unit addWeapon _pistol;
		[_unit,_rifleMag,3] call ACE_fnc_PackMagazine;
		[_unit,_mmgMag,3] call ACE_fnc_PackMagazine;
		call _RIFM_IFAK;
	};
	
	// -- Vehicle Crews --
	// Pilot
	case "pilot" : {
		_unit addWeapon _radio;
		_unit addWeapon _radioRuck;
		for "_i" from 1 to 8 do {_unit addMagazine _smgMag};
		for "_i" from 1 to 4 do {_unit addMagazine _smoke};
		for "_i" from 1 to 3 do {_unit addMagazine _pistolMag};
		_unit addWeapon _smg;
		_unit addWeapon _pistol;
		{ _unit addMagazine _x } foreach _IFAK;
		[_unit, true] call ACE_fnc_setCrewProtection;
	};
	// crewmans
	case "crewman" : {
		_unit addWeapon _radio;
		for "_i" from 1 to 8 do {_unit addMagazine _smgMag};
		for "_i" from 1 to 4 do {_unit addMagazine _smoke};
		for "_i" from 1 to 3 do {_unit addMagazine _pistolMag};
		_unit addWeapon _smg;
		_unit addWeapon _pistol;
		{ _unit addMagazine _x } foreach _IFAK;
		[_unit, true] call ACE_fnc_setCrewProtection;
	};
	
	// -- Weapons Teams --
	// HMG Gunner
	case "hmggun" : {
		_unit addWeapon _hmgBarrel;
		for "_i" from 1 to 8 do {_unit addMagazine _rifleMag};
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		_unit addWeapon _rifle;
		{ _unit addMagazine _x } foreach _IFAK;
	};
	// HMG Assistant
	case "hmgass" : {
		_unit addWeapon _hmgTripod;
		for "_i" from 1 to 7 do {_unit addMagazine _rifleMag};
		for "_i" from 1 to 1 do {_unit addMagazine _hmgMag};
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		_unit addWeapon _rifle;
		{ _unit addMagazine _x } foreach _IFAK;
	};
	// HMG Ammobearer
	case "hmgammo" : {
		_unit addWeapon _plebRuck;
		for "_i" from 1 to 10 do {_unit addMagazine _rifleMag};
		for "_i" from 1 to 2 do {_unit addMagazine _hmgMag};
		_unit addWeapon _rifle;
		call _RIFM_IFAK;
	};
	// MAT Gunner
	case "matgun" : {
		if (player == _unit) then {
			_unit addWeapon _rpg;
			waitUntil {_wob = [_unit, _rpg] call ACE_fnc_PutWeaponOnBack; _wob};
			_unit addWeapon _plebRuck;
		};
		for "_i" from 1 to 8 do {_unit addMagazine _rifleMag};
		{ _unit addMagazine _x } foreach _rpgMags;
		_unit addWeapon _rifle;
		{[_unit,_x,1] call ACE_fnc_PackMagazine} foreach _rpgMags;
		[_unit,_rifleMag,4] call ACE_fnc_PackMagazine;
		{[_unit,_x,2] call ACE_fnc_PackMagazine} foreach _throwG;
		{ _unit addMagazine _x } foreach _IFAK;
	};
	// MAT Ammobearer
	case "matammo" : {
		_unit addWeapon _plebRuck;
		_unit addWeapon _rangeFinder;
		for "_i" from 1 to 7 do {_unit addMagazine _rifleMag};
		_unit addMagazine _rangeFinderBat;
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		_unit addWeapon _rifle;
		{[_unit,_x,2] call ACE_fnc_PackMagazine} foreach _rpgMags;
		[_unit,_rifleMag,4] call ACE_fnc_PackMagazine;
		{[_unit,_x,2] call ACE_fnc_PackMagazine} foreach _throwG;
		[_unit,_rangeFinderBat,1] call ACE_fnc_PackMagazine;
		{ _unit addMagazine _x } foreach _IFAK;
	};
	// HAT Gunner
	case "hatgun" : {
		_unit addWeapon _hat;
		for "_i" from 1 to 8 do {_unit addMagazine _rifleMag};
		_unit addMagazine _hatMag;
		for "_i" from 1 to 3 do {_unit addMagazine _pistolMag};
		_unit addWeapon _rifle;
		_unit addWeapon _pistol;
		{ _unit addMagazine _x } foreach _IFAK;
	};
	// HAT Ammobearer
	case "hatammo" : {
		_unit addWeapon _plebRuck;
		_unit addWeapon _rangeFinder;
		for "_i" from 1 to 7 do {_unit addMagazine _rifleMag};
		_unit addMagazine _rangeFinderBat;
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 3 do {_unit addMagazine _pistolMag};
		_unit addWeapon _rifle;
		_unit addWeapon _pistol;
		[_unit,_hatMag,1] call ACE_fnc_PackMagazine;
		[_unit,_rifleMag,6] call ACE_fnc_PackMagazine;
		{[_unit,_x,2] call ACE_fnc_PackMagazine} foreach _throwG;
		[_unit,_rangeFinderBat,1] call ACE_fnc_PackMagazine;
		{ _unit addMagazine _x } foreach _IFAK;
	};
	// if undefined or incorrectly defined, give hint and assign standard rifleman gear
	default {
		_unit sideChat format ["No or incorrectly defined loadout for %1",_unit];
		_unit addWeapon _plebRuck;
		for "_i" from 1 to 8 do {_unit addMagazine _rifleMag};
		{ _unit addMagazine _x; _unit addMagazine _x } foreach _throwG;
		for "_i" from 1 to 3 do {_unit addMagazine _pistolMag};
		_unit addWeapon _rifle;
		_unit addWeapon _pistol;
		[_unit,_rifleMag,8] call ACE_fnc_PackMagazine;
		{[_unit,_x,2] call ACE_fnc_PackMagazine} foreach _throwG;
		call _RIFM_IFAK;
		[_unit,_autoRifleMag,2] call ACE_fnc_PackMagazine;
	};
};

_unit selectWeapon (primaryWeapon _unit);
sleep _delay;
_unit GroupChat format ["%1 completed gear assign", name _unit];